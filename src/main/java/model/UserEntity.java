package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "hibernate_test_db")
@IdClass(UserEntityPK.class)
public class UserEntity {
    private int id;
    private String firstName;
    private String secondName;
    private int passwordId;

    public UserEntity() {
    }

    public UserEntity(int id, String firstName, String secondName, int passwordId) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.passwordId = passwordId;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "second_name")
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Id
    @Column(name = "password_id")
    public int getPasswordId() {
        return passwordId;
    }

    public void setPasswordId(int passwordId) {
        this.passwordId = passwordId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (id != that.id) return false;
        if (passwordId != that.passwordId) return false;
        if (!Objects.equals(firstName, that.firstName)) return false;
        return Objects.equals(secondName, that.secondName);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + passwordId;
        return result;
    }
}
