package DAO;

import model.FileEntity;

interface FileDAO extends GeneralDAO<FileEntity, String> {
}
