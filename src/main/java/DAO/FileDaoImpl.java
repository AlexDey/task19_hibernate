package DAO;

import model.FileEntity;
import persistant.ConnectionManager;
import transformer.Transformer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FileDaoImpl implements FileDAO {
    private static final String FIND_ALL = "SELECT * FROM file";
    private static final String DELETE = "DELETE FROM file WHERE id=?";
    private static final String CREATE = "INSERT file (id, file_name, url, size) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE file SET file_name=?, url=?, size=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM file WHERE id=?";


    @Override
    public List<FileEntity> findAll() throws SQLException {
        List<FileEntity> departments = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    departments.add((FileEntity) new Transformer(FileEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return departments;
    }

    @Override
    public FileEntity findById(String id) throws SQLException {
        FileEntity entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (FileEntity) new Transformer(FileEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(FileEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
//            ps.setString(1,entity.getDeptNo());
//            ps.setString(2,entity.getDeptName());
//            ps.setString(3,entity.getLocation());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(FileEntity entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
//            ps.setString(1,entity.getDeptName());
//            ps.setString(2,entity.getLocation());
//            ps.setString(3,entity.getDeptNo());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(String id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setString(1, id);
            return ps.executeUpdate();
        }
    }

}
