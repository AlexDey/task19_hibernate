package model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "file", schema = "hibernate_test_db")
public class FileEntity {
    private int id;
    private String fileName;
    private String url;
    private int size;

    public FileEntity() {
    }

    public FileEntity(int id, String fileName, String url, int size) {
        this.id = id;
        this.fileName = fileName;
        this.url = url;
        this.size = size;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "file_name")
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "size")
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FileEntity that = (FileEntity) o;

        if (id != that.id) return false;
        if (size != that.size) return false;
        if (!Objects.equals(fileName, that.fileName)) return false;
        return Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + size;
        return result;
    }
}
