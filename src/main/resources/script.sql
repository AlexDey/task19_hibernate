
insert into password values (1, '11111');
insert into password values (2, '22222');
insert into password values (3, '33333');
insert into password values (4, '44444');
insert into password values (5, '55555');

insert into user values (1, 'Alex', 'Dir', 3);
insert into user values (2, 'Serg', 'Ole', 2);
insert into user values (3, 'Frank', 'Sinatra', 1);
insert into user values (4, 'Big', 'Foot', 5);
insert into user values (5, 'Nick', 'Nelson', 4);

insert into file values (1, 'file_1', 'url_1', 15);
insert into file values (2, 'file_2', 'url_2', 34);
insert into file values (3, 'file_3', 'url_3', 142);
insert into file values (4, 'file_4', 'url_4', 14);
insert into file values (5, 'file_5', 'url_5', 55);
insert into file values (6, 'file_6', 'url_6', 85);

insert into user_has_file values (1, 3, 5);
insert into user_has_file values (2, 2, 4);
insert into user_has_file values (3, 1, 3);
insert into user_has_file values (4, 5, 6);
insert into user_has_file values (5, 4, 5);
insert into user_has_file values (3, 1, 1);
insert into user_has_file values (4, 5, 2);
insert into user_has_file values (4, 5, 3);