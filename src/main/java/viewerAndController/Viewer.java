package viewerAndController;

import model.FileEntity;
import service.FileService;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Viewer {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Viewer() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", "   1 - Table: File");
        menu.put("11", "  11 - Create for File");
        menu.put("12", "  12 - Update File");
        menu.put("13", "  13 - Delete from File");
        menu.put("14", "  14 - Select File");
        menu.put("15", "  15 - Find File by ID");

        menu.put("Q", "   Q - exit");

        methodsMenu.put("11", this::createForFileTable);
        methodsMenu.put("12", this::updateFileTable);
        methodsMenu.put("13", this::deleteFromFileTable);
        methodsMenu.put("14", this::selectFileTable);
        methodsMenu.put("15", this::findFileByID);
    }

    private void createForFileTable() throws SQLException {
        System.out.println("Input file ID: ");
        int id = input.nextInt();
        System.out.println("Input file name: ");
        String name = input.nextLine();
        System.out.println("Input file URL: ");
        String url = input.nextLine();
        System.out.println("Input file size: ");
        int size = input.nextInt();

        FileEntity fileEntity = new FileEntity(id, name, url, size);
        FileService fileService = new FileService();
        int count = fileService.create(fileEntity);
        System.out.printf("There are created %d rows\n", count);
    }

    private void updateFileTable() throws SQLException {
        System.out.println("Input file ID: ");
        int id = input.nextInt();
        System.out.println("Input file name: ");
        String name = input.nextLine();
        System.out.println("Input file URL: ");
        String url = input.nextLine();
        System.out.println("Input file size: ");
        int size = input.nextInt();

        FileEntity fileEntity = new FileEntity(id, name, url, size);
        FileService fileService = new FileService();
        int count = fileService.create(fileEntity);
        System.out.printf("There are updated %d rows\n", count);
    }

    private void deleteFromFileTable() throws SQLException {
        System.out.println("Input ID: ");
        String id = input.nextLine();
        FileService departmentService = new FileService();
        int count = departmentService.delete(id);
        System.out.printf("There are deleted %d rows\n", count);
    }

    private void selectFileTable() throws SQLException {
        System.out.println("\nTable: File");
        FileService fileService = new FileService();
        List<FileEntity> departments = fileService.findAll();
        for (FileEntity entity : departments) {
            System.out.println(entity);
        }
    }

    private void findFileByID() throws SQLException {
        System.out.println("Input ID: ");
        String id = input.nextLine();
        FileService departmentService = new FileService();
        FileEntity entity = departmentService.findById(id);
        System.out.println(entity);
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet())
            if (key.length() == 1) System.out.println(menu.get(key));
    }

    private void outputSubMenu(String fig) {

        System.out.println("\nSubMENU:");
        for (String key : menu.keySet())
            if (key.length() != 1 && key.substring(0, 1).equals(fig)) System.out.println(menu.get(key));
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point:");
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                System.out.println("Please, select menu point:");
                keyMenu = input.nextLine().toUpperCase();
            }

            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
