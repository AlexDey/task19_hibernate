package model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class UserEntityPK implements Serializable {
    private int id;
    private int passwordId;

    public UserEntityPK() {
    }

    public UserEntityPK(int id, int passwordId) {
        this.id = id;
        this.passwordId = passwordId;
    }

    @Column(name = "id")
    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "password_id")
    @Id
    public int getPasswordId() {
        return passwordId;
    }

    public void setPasswordId(int passwordId) {
        this.passwordId = passwordId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntityPK that = (UserEntityPK) o;

        if (id != that.id) return false;
        return passwordId == that.passwordId;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + passwordId;
        return result;
    }
}
