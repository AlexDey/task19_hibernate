package service;

import DAO.FileDaoImpl;
import model.FileEntity;

import java.sql.SQLException;
import java.util.List;

public class FileService {
    public List<FileEntity> findAll() throws SQLException {
        return new FileDaoImpl().findAll();
    }

    public FileEntity findById(String id) throws SQLException {
        return new FileDaoImpl().findById(id);
    }

    public int create(FileEntity entity) throws SQLException {
        return new FileDaoImpl().create(entity);
    }

    public int update(FileEntity entity) throws SQLException {
        return new FileDaoImpl().update(entity);
    }

    public int delete(String id) throws SQLException {
        return new FileDaoImpl().delete(id);
    }
}
