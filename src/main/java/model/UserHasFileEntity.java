package model;

import javax.persistence.*;

@Entity
@Table(name = "user_has_file", schema = "hibernate_test_db")
@IdClass(UserHasFileEntityPK.class)
public class UserHasFileEntity {
    private int userId;
    private int userPasswordId;
    private int fileId;

    public UserHasFileEntity() {
    }

    public UserHasFileEntity(int userId, int userPasswordId, int fileId) {
        this.userId = userId;
        this.userPasswordId = userPasswordId;
        this.fileId = fileId;
    }

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "user_password_id")
    public int getUserPasswordId() {
        return userPasswordId;
    }

    public void setUserPasswordId(int userPasswordId) {
        this.userPasswordId = userPasswordId;
    }

    @Id
    @Column(name = "file_id")
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserHasFileEntity that = (UserHasFileEntity) o;

        if (userId != that.userId) return false;
        if (userPasswordId != that.userPasswordId) return false;
        return fileId == that.fileId;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + userPasswordId;
        result = 31 * result + fileId;
        return result;
    }
}
